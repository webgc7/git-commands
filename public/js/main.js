let msHeader = document.getElementById("header").contentWindow;
let navigatorType = navigator.userAgent;
let browserType;

function start() {
   /* Get current date */
   let e = new Date().getFullYear();
   document.getElementById("copyright").textContent += e;
   getNavigator();
   window.addEventListener("message", getMessageX, false);
}

/* Obtener tipo de navegador */
function getNavigator() {
   if (navigatorType.indexOf("Chrome") > -1) {
      browserType = "chrome";
   } else if (navigatorType.indexOf("Safari") > -1) {
      browserType = "safari";
   } else if (navigatorType.indexOf("Opera") > -1) {
      browserType = "opera";
   } else if (navigatorType.indexOf("Firefox") > -1) {
      browserType = "firefox";
   } else if (navigatorType.indexOf("MSIE") > -1) {
      browserType = "msie";
   }
}

/* Pantalla completa  webkit */
function fullScreenWebkit() {
   document.documentElement.webkitRequestFullscreen();
   msHeader.postMessage("minimize", "*");
}

/* Pantalla normal */
function normalScreeWebkit() {
   document.webkitExitFullscreen();
   msHeader.postMessage("maximize", "*");
}

function getMessageX(ms) {
   /* console.log(ms.origin, typeof ms.data, ms.data); */
   let message = ms.data;
   if (message === "eventScreen") {
      try {
         switch (browserType) {
            case "chrome":
               if (!document.webkitFullscreenElement) {
                  fullScreenWebkit();
               } else {
                  normalScreeWebkit();
               }
               break;
            case "opera":
               if (!document.webkitFullscreenElement) {
                  fullScreenWebkit();
               } else {
                  normalScreeWebkit();
               }
               break;
            case "edge":
               if (!document.webkitFullscreenElement) {
                  fullScreenWebkit();
               } else {
                  normalScreeWebkit();
               }
               break;
            case "safari":
               if (!document.webkitFullscreenElement) {
                  fullScreenWebkit();
               } else {
                  normalScreeWebkit();
               }
               break;
            case "firefox":
               if (!document.mozFullScreenElement) {
                  document.documentElement.requestFullscreen();
                  msHeader.postMessage("minimize", "*");
               } else {
                  document.mozCancelFullScreen();
                  msHeader.postMessage("maximize", "*");
               }
               break;
         }
      } catch (e) {
         alert(e);
      }
   }
}

window.addEventListener("load", start);
